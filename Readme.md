﻿Kerl PHP
===================


This is the PHP implementation of Kerl. It requires the  [php gmp](http://php.net/manual/en/book.gmp.php) extension to work right.

----------

> **Note:** In order for Keccak 384 to work according to Kerl's specification. You need to edit line 51 and change '0x06' to'0x01' in SHA3.php inside of PHP-SHA3-Streamable after cloning this repo!
